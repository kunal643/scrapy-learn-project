import scrapy


class ProductSinglePageSpider(scrapy.Spider):
    name = 'product_single_page'
    start_urls = [
        'https://www.amazon.com/Altavoz-Bluetooth-Doss-SoundBox-Negro/dp/B01CQOV3YO/ref=bbp_bb_a77114_st_8174_w_0?psc=1&smid=A1SJW9CH59LQ7Z']

    def parse(self, response):
        for price in response.css('span.a-size-medium.a-color-price'):
            yield {'price': price.css('span::text').get().strip()}

        for image in response.css('div.imgTagWrapper>img'):
            yield {'image': image.attrib['src']}

        for title in response.css('#title>#productTitle.a-size-large::text'):
            yield {'title': title.get().strip()}

        for description in response.css('#feature-bullets>ul.a-unordered-list>li>span.a-list-item::text'):
            yield {'description': description.get().strip()}


class BlogSpider(scrapy.Spider):
    name = 'blogspider'
    #start_urls = ['https://blog.scrapinghub.com']
    start_urls = [
        'https://www.amazon.com/Altavoz-Bluetooth-Doss-SoundBox-Negro/dp/B01CQOV3YO/ref=bbp_bb_a77114_st_8174_w_0?psc=1&smid=A1SJW9CH59LQ7Z']

    def parse(self, response):
        for price in response.css('span.a-size-medium.a-color-price'):
            yield {'price': price.css('span::text').get().strip()}

        for image in response.css('div.imgTagWrapper>img'):
            yield {'image': image.attrib['src']}

        for title in response.css('#title>#productTitle.a-size-large::text'):
            yield {'title': title.get().strip()}

        for description in response.css('#feature-bullets>ul.a-unordered-list>li>span.a-list-item::text'):
            yield {'description': description.get().strip()}

        # for next_page in response.css('a.next-posts-link'):
        #    yield response.follow(next_page, self.parse)
